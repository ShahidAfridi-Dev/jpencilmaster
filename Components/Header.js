import { useState } from "react";
import { motion, AnimatePresence } from "framer-motion";
import Link from "next/link";
import Image from "next/image";
import {
  FaBullhorn,
  FaCode,
  FaMobileAlt,
  FaPaintBrush,
  FaChevronDown,
  FaPalette,
  FaUser,
  FaFileInvoiceDollar,
  FaBookOpen,
  FaBars,
  FaTimes,
} from "react-icons/fa"; // Import additional icons for the resources submenu

const Header = () => {
  const [isSolutionsOpen, setIsSolutionsOpen] = useState(false);
  const [isResourcesOpen, setIsResourcesOpen] = useState(false); // New state for the Resources submenu
  const [isMobileMenuOpen, setIsMobileMenuOpen] = useState(false);
  const [isMobileSolutionsOpen, setMobileSolutionsOpen] = useState(false);
  const [isMobileResourcesOpen, setMobileResourcesOpen] = useState(false);

  // Menu items for Solutions with their corresponding icons and text
  const solutionsMenuItems = [
    {
      icon: FaBullhorn,
      text: "DATA CONTENT 1",
      href: "/social-media",
      color: "text-red-500",
    },
    {
      icon: FaCode,
      text: "DATA CONTENT 2",
      href: "/web-design",
      color: "text-green-500",
    },
    {
      icon: FaMobileAlt,
      text: "DATA CONTENT 3",
      href: "/illustration",
      color: "text-blue-500",
    },
    {
      icon: FaPaintBrush,
      text: "DATA CONTENT 4",
      href: "/presentation",
      color: "text-yellow-500",
    },
  ];

  // New Menu items for Resources with their corresponding icons and text
  const resourcesMenuItems = [
    {
      icon: FaPalette,
      text: "DATA CONTENT 1",
      href: "/free-illustrations",
      color: "text-indigo-500",
    },
    {
      icon: FaBookOpen,
      text: "DATA CONTENT 2",
      href: "/guides",
      color: "text-pink-500",
    },
    {
      icon: FaMobileAlt,
      text: "DATA CONTENT 3",
      href: "/design-blog",
      color: "text-purple-500",
    },
    {
      icon: FaFileInvoiceDollar,
      text: "DATA CONTENT 4",
      href: "/case-studies",
      color: "text-green-500",
    },
  ];

  const containerVariants = {
    hidden: { opacity: 1, scale: 0.9 },
    visible: {
      opacity: 1,
      scale: 1,
      transition: {
        when: "beforeChildren",
        staggerChildren: 0.1, // Staggering for a wave-like effect
      },
    },
  };

  const itemVariants = {
    hidden: { y: 20, opacity: 0, rotate: -10 },
    visible: {
      y: 0,
      opacity: 1,
      rotate: 0,
      transition: {
        type: "spring", // Using spring for smooth motion
        stiffness: 70, // Reduced stiffness for a gentler spring
        damping: 12, // Slightly increased damping for control
        duration: 0.6, // Shorter duration for each item's animation
      },
    },
  };

  // Function to close the mobile menu
  const closeMobileMenu = () => setIsMobileMenuOpen(false);
  return (
   
    <header className="bg-white shadow-md font-ubuntu">
      <div className="container mx-auto flex items-center justify-between p-4">
        {/* Logo */}
        <Link href="/">
          <span className="flex items-center">
            <Image
              src="/assets/logo.png" // Replace with your logo image path
              alt="Logo"
              width={250} // Adjust to your logo size
              height={50} // Adjust to your logo size
              priority // Preload the image on the initial load
            />
          </span>
        </Link>
        {/* Hamburger Icon */}
       

        {/* Navigation */}
        <motion.nav
          className="hidden lg:flex flex-col justify-center flex-grow mt-4 sm:mt-0"
          variants={containerVariants}
          initial="hidden"
          animate="visible"
        >
          <motion.div
            className="flex justify-center items-center space-x-4"
            variants={containerVariants}
          >
            {/* Iterate over each nav item */}
            {[
  { name: "HOME", path: "/" },
  { name: "SERVICES", path: "/Service" },
  { name: "SHOP", path: "/shop" },
  { name: "PRICING", path: "/Pricing" }, // Update this line with the correct path for pricing
  { name: "ABOUT US", path: "/about" },
  { name: "CONTACT US", path: "/contact" },
].map((item, index) => (
  <Link href={item.path} key={index}>
    <motion.span
      className="text-gray-600 text-sm xl:text-lg hover:text-gray-800 font-medium px-4 py-2 rounded-md"
      variants={itemVariants}
    >
      {item.name}
    </motion.span>
  </Link>
))}
          </motion.div>
        </motion.nav>
        <Link href="/login">
          <span className="hidden lg:flex items-center justify-end flex-grow">
            <FaUser className="text-lg mr-2" size={24} />
            Login
          </span>
        </Link>
        <div className="flex lg:hidden">
          <button onClick={() => setIsMobileMenuOpen(!isMobileMenuOpen)}>
            <FaBars className="text-2xl" />
          </button>
        </div>
        {/* Mobile Menu */}
        <AnimatePresence>
          {isMobileMenuOpen && (
            <motion.div
              className="fixed inset-0 z-10 bg-white p-8 overflow-auto"
              initial={{ x: "100%" }}
              animate={{ x: 0 }}
              exit={{ x: "100%" }}
              transition={{ type: "spring", stiffness: 75 }}
            >
              {/* Close icon */}
              {/* Close icon */}
              <button
                onClick={closeMobileMenu}
                className="absolute top-3 right-4"
                whileHover={{ scale: 1.1 }}
                whileTap={{ scale: 0.9 }}
                style={{
                  background: "linear-gradient(135deg, #6D5BBA, #8D58BF)",
                  // boxShadow: '0px 2px 15px rgba(109, 91, 186, 0.5)',
                  borderRadius: "50%",
                  padding: "0.5rem",
                }}
              >
                <motion.div
                  className="text-lg"
                  style={{ color: "#FFFFFF" }}
                  initial={{ scale: 1 }}
                  animate={{ scale: 1 }}
                  whileHover={{ scale: 1.1, rotate: 90 }}
                  whileTap={{ scale: 0.9, rotate: 180 }}
                  transition={{ type: "spring", stiffness: 300 }}
                >
                  <FaTimes />
                </motion.div>
              </button>

              {/* Mobile navigation links */}
              {/* SOLUTIONS Dropdown */}

              {/* Other Navigation Links */}
              <Link href="/our-work">
                <span
                  className="flex items-center p-4 border-b text-gray-600 hover:bg-gray-100 font-medium"
                  onClick={closeMobileMenu}
                >
                  HOME
                </span>
              </Link>
              <Link href="/our-work">
                <span
                  className="flex items-center p-4 border-b text-gray-600 hover:bg-gray-100 font-medium"
                  onClick={closeMobileMenu}
                >
                  ABOUT US
                </span>
              </Link>
              <Link href="/our-work">
                <span
                  className="flex items-center p-4 border-b text-gray-600 hover:bg-gray-100 font-medium"
                  onClick={closeMobileMenu}
                >
                  SHOP
                </span>
              </Link>
              <Link href="/Pricing">
                <span
                  className="flex items-center p-4 border-b text-gray-600 hover:bg-gray-100 font-medium"
                  onClick={closeMobileMenu}
                >
                  PRICING
                </span>
              </Link>
              <Link href="/pricing">
                <span
                  className="flex items-center p-4 border-b text-gray-600 hover:bg-gray-100 font-medium"
                  onClick={closeMobileMenu}
                >
                  CONTACT US
                </span>
              </Link>
              <Link href="/login">
                <span
                  className="flex items-center p-4 border-b text-gray-600 hover:bg-gray-100 font-medium"
                 
                >
                  LOGIN
                </span>
              </Link>
             
            </motion.div>
          )}
        </AnimatePresence>
        {/* Action Buttons */}
      </div>
    </header>
    
  );
};

export default Header;
