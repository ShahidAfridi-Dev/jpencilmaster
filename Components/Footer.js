import React from 'react';
import { motion } from 'framer-motion';
import { FaFacebookF, FaTwitter, FaYoutube } from 'react-icons/fa';
import Link from "next/link";
import Image from "next/image";
const listVariant = {
  hidden: { opacity: 0, y: 20 },
  show: {
    opacity: 1,
    y: 0,
    transition: {
      ease: 'easeOut',
      duration: 0.3
    }
  }
};

const ListItem = ({ children }) => {
  return (
    <motion.li
      variants={listVariant}
      initial="hidden"
      whileInView="show"
      viewport={{ once: true }}
      className="text-md py-1 hover:text-gray-900 cursor-pointer transition-colors duration-300"
    >
      {children}
    </motion.li>
  );
};

const Footer = () => {
  return (
    <footer className="bg-white font-poppins text-gray-800 py-12">
      <div className="max-w-7xl mx-auto px-4 lg:flex justify-between  items-start">
        
        <div className="">
        <Link href="/">
          <span className="flex items-center justify-center lg:justify-start">
            <Image
              src="/assets/logo.png" // Replace with your logo image path
              alt="Logo"
              width={250} // Adjust to your logo size
              height={50} // Adjust to your logo size
              priority // Preload the image on the initial load
            />
          </span>
        </Link>
          <h2 className="text-xl text-center lg:text-start font-bold mb-4">J Pencil Strives to unite the world of Art into one family.</h2>
          <div className="flex justify-center lg:justify-start space-x-4 mb-4">
            <a href="#" className="text-gray-600 hover:text-gray-900"><FaFacebookF size={20} /></a>
            <a href="#" className="text-gray-600 hover:text-gray-900"><FaTwitter size={20} /></a>
            <a href="#" className="text-gray-600 hover:text-gray-900"><FaYoutube size={20} /></a>
          </div>
          <p className="text-lg font-semibold font-maven text-center lg:text-left">Powered by JPencil.</p>
        </div>

        <div className="flex flex-wrap justify-between lg:pl-8 lg:w-3/4 mt-12 lg:mt-0">
          <div className="w-1/2 lg:w-1/4 mb-8 lg:mb-0">
            <h3 className="font-semibold text-xl mb-2">SERVICES</h3>
            <motion.ul
              initial="hidden"
              whileInView="show"
              viewport={{ once: true, amount: 0.8 }}
              className="space-y-1"
            >
              <ListItem>GOLD PLANS</ListItem>
              <ListItem>BID & ASK</ListItem>
              <ListItem>BUY & SELL</ListItem>
              <ListItem>OWN PRODUCTS</ListItem>
          
            </motion.ul>
          </div>

          <div className="w-1/2 lg:w-1/4 mb-8 lg:mb-0">
            <h3 className="font-semibold text-xl mb-2">FEATURES</h3>
            <motion.ul
              initial="hidden"
              whileInView="show"
              viewport={{ once: true, amount: 0.8 }}
              className="space-y-1"
            >
               <Link href="/Gallery">
      <span>
        <ListItem>Gallery</ListItem>
      </span>
    </Link>
              <ListItem>Custom Pages</ListItem>
              <ListItem>Widgets</ListItem>
              <ListItem>Website Customization</ListItem>
              
            </motion.ul>
          </div>

          <div className="w-1/2 lg:w-1/4 mb-8 lg:mb-0">
            <h3 className="font-semibold mb-2 text-xl">LINKS</h3>
            <motion.ul
              initial="hidden"
              whileInView="show"
              viewport={{ once: true, amount: 0.8 }}
              className="space-y-1"
            >
              <ListItem>About Us</ListItem>
              <ListItem>Contact Us</ListItem>
              <ListItem>Blogs</ListItem>
              <ListItem>Insights</ListItem>
            </motion.ul>
          </div>
        </div>
      </div>
      <div className="text-center mt-8 border-t pt-4">
        <a href="#" className="hover:underline text-gray-600 hover:text-gray-900 mx-2">Privacy Policy</a>
        <span className="text-gray-300">|</span>
        <a href="#" className="hover:underline text-gray-600 hover:text-gray-900 mx-2">Our Terms</a>
      </div>
    </footer>
  );
};

export default Footer;
