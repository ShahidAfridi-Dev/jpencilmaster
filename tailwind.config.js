/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    './pages/**/*.{js,ts,jsx,tsx,mdx}',
    './components/**/*.{js,ts,jsx,tsx,mdx}',
    './app/**/*.{js,ts,jsx,tsx,mdx}',
  ],
  theme: {
    extend: {
      colors: {
        'gold': '#D8B889', // You can name the color whatever you like
      },
      fontFamily: {
        poppins: ['Poppins', 'sans-serif'],
        ubuntu: ['Ubuntu', 'sans-serif'],
        space: ['Space Grotesk', 'sans-serif'],
        maven: ['Maven Pro', 'sans-serif'],
        overlock: ['Overlock SC', 'sans-serif'],
        arabic:['Cormorant Garamond','serif'],
      },
    },
  },
  plugins: [],
}
