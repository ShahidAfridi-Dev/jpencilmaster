import { Html, Head, Main, NextScript } from 'next/document'

export default function Document() {
  return (
    <Html lang="en">
     <Head>
      <link rel="preconnect" href="https://fonts.googleapis.com"/>
<link rel="preconnect" href="https://fonts.gstatic.com" crossOrigin={true}/>
<link href="https://fonts.googleapis.com/css2?family=Maven+Pro&family=Overlock+SC&family=Poppins:wght@300;500&family=Space+Grotesk:wght@300&family=Ubuntu:wght@500&display=swap" rel="stylesheet"/>

<link href="https://fonts.googleapis.com/css2?family=Cormorant+Garamond:wght@600;700&family=Poppins:wght@500&display=swap" rel="stylesheet"></link>
        </Head>
      <body>
        <Main />
        <NextScript />
      </body>
    </Html>
  )
}
