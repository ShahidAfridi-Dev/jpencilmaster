import { motion, AnimatePresence } from 'framer-motion';

const Counter = ({ count, label }) => {
  return (
    <motion.div
      className="border-2 border-yellow-400 p-4 rounded-lg flex flex-col items-center justify-center"
      initial={{ opacity: 0, y: 50 }}
      animate={{ opacity: 1, y: 0 }}
      transition={{ duration: 0.5 }}
    >
      <AnimatePresence>
        <motion.span
          className="text-4xl font-bold"
          initial={{ opacity: 0 }}
          animate={{ opacity: 1 }}
          exit={{ opacity: 0 }}
          key={count}
        >
          {count}
        </motion.span>
      </AnimatePresence>
      <span className="text-lg">{label}</span>
    </motion.div>
  );
};

const Community = () => {
  return (
    <div className="flex justify-center items-center flex-col">
      <div className="text-3xl font-bold mb-4">J Pencil Community</div>
      <div className="grid grid-cols-3 gap-4">
        <Counter count={380} label="artists" />
        <Counter count={200} label="shops" />
        <Counter count={150} label="cities" />
      </div>
      <motion.div
        className="border-t-4 border-yellow-400 p-4 mt-4 rounded-lg"
        initial={{ opacity: 0 }}
        animate={{ opacity: 1 }}
        transition={{ delay: 1.5, duration: 0.5 }}
      >
        <Counter count={2000} label="Products" />
      </motion.div>
    </div>
  );
};

export default Community;
