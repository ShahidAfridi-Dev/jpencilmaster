import React, { useState } from "react";
import {
  FaLightbulb,
  FaBookOpen,
  FaPaintBrush,
  FaRocket,
} from "react-icons/fa";
import { FaHandsHelping, FaUsers, FaStore } from "react-icons/fa";
import { motion, useAnimation } from "framer-motion";
import { useInView } from "react-intersection-observer";

// Define the steps data with an additional field for images
const steps = [
  {
    id: 1,
    icon: <FaHandsHelping className="text-red-500" />,
    title: "Empowering Artisans",
    description:
      "J Pencil Strives to unite the world of Art into one family and give every single artist in the globe an equal opportunity to sell their masterpieces and beautify the world.We at J Pencil, believe that every artisan can have their work glorified at every corner of the globe with us",
    image: "/assets/motii1.jpeg",
  },
  {
    id: 2,
    icon: <FaUsers className="text-blue-500" />,
    title: "Connecting Entrepreneurs",
    description:
      "J Pencil aims to connect artisans and crafters in order to help them take their businesses to the next level and become more well-known.Artisans can now broadcast their products and talents to a larger audience thanks to our technology.Our motive is to help our young talents to strive in their life",
    image: "/assets/moti2.jpeg",
  },
  {
    id: 3,
    icon: <FaStore className="text-green-500" />,
    title: "Redefining Retail",
    description:
      "The retail industry is a fiercely competitive one, and we're here to help you get started with your own online store. It is not as difficult as you may believe. You can reach a larger group of people and grow your business with our J Pencil. Its goal is to reshape and redefine the retail industry of art products.",
    image: "/assets/moti3.jpeg",
  },
  {
    id: 4,
    icon: <FaRocket className="text-purple-500" />,
    title: "Launch Goal",
    description:
      "At J Pencil, we understand that launching a goal is like setting forth on a rocket journey. It requires careful planning, precise execution, and unwavering determination. We specialize in helping you set and achieve ambitious goals with precision and determination. Our innovative strategies redefine the path to success, guiding you through every stage of your journey to ensure you reach new heights. Whether it's launching a new project, a business venture, or a personal milestone, we're here to propel you toward your goals with confidence and expertise.",
    image: "/assets/moti5.jpeg",
  },
];

const StepNavigation = ({ steps, currentStep, setCurrentStep }) => {
  return (
    <div className="flex justify-around">
      {steps.map((step) => (
        <button
          key={step.id}
          className={`flex flex-col items-center transition duration-300 ease-in-out ${
            currentStep === step.id
              ? "text-indigo-600"
              : "text-gray-500 hover:text-indigo-500"
          }`}
          onClick={() => setCurrentStep(step.id)}
        >
          {/* Increase padding and provide a fixed size for the icon container */}
          <div className="p-3 rounded-full bg-white shadow-lg text-3xl">
            {" "}
            {/* Adjust text size here for icon size */}
            {React.cloneElement(step.icon, { className: "w-10 h-10" })}{" "}
            {/* Adjust width and height here */}
          </div>
          <span className="mt-2 text-xs sm:text-sm lg:text-lg">
            {step.title}
          </span>{" "}
          {/* Adjust text size here if necessary */}
        </button>
      ))}
    </div>
  );
};

const MotivationContent = ({ step }) => {
  return (
    <div className="flex-1">
      <div className="text-lg text-gray-700 mb-4 lg:text-center text-justify">
        <span className="text-purple-700 text-2xl lg:text-6xl font-bold">{`0${step.id}.`}</span>
        <span className="text-2xl lg:text-4xl font-bold ml-4 ">
          {step.title}
        </span>
      </div>
      <p className="text-xl text-gray-600 mb-8 lg:text-center text-justify">
        {step.description}
      </p>
    </div>
  );
};
const Motivation = () => {
  // Animation controls
  const controls = useAnimation();
  const { ref, inView } = useInView({
    triggerOnce: true,
    threshold: 0.5, // Adjust as needed
  });

  React.useEffect(() => {
    if (inView) {
      controls.start("visible");
    }
  }, [controls, inView]);

  const stepVariants = {
    hidden: { opacity: 0, y: 50 },
    visible: {
      opacity: 1,
      y: 0,
      transition: { delay: 0.5, duration: 1 },
    },
  };
  const [currentStep, setCurrentStep] = useState(steps[0].id);

  const currentStepData = steps.find((step) => step.id === currentStep);

  return (
    <motion.div  className="bg-gray-100 py-12 font-maven"
      initial="hidden"
      animate={controls}
      variants={stepVariants}
      ref={ref}>
      <div className="container mx-auto px-6">
        <div
          className="w-full lg:w-auto
    
    "
        >
          <StepNavigation
            steps={steps}
            currentStep={currentStep}
            setCurrentStep={setCurrentStep}
          />
        </div>
        {/* Step Navigation and Content Container /} */}
        <div>
          {/* {/ Step Navigation */}

          {/* Content & Image /} */}
          <div className="flex-1 my-5 p-5">
            <MotivationContent step={currentStepData} />
            {/* {/ Image will be here */}
            <div className="mt-6 flex justify-center">
              <img
                src={currentStepData.image}
                alt={`Step ${currentStepData.title}`}
                className="w-[300px] h-[200px] md:w-full md:h-auto max-w-xl rounded-lg shadow-lg"
              />
            </div>
          </div>
        </div>
      </div>
    </motion.div>
  );
};

export default Motivation;
