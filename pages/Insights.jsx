import React, { useEffect } from "react";
import {
  FaSearch,
  FaRegEnvelope,
  FaFacebook,
  FaTwitter,
  FaLinkedin,
  FaShareAlt,
  FaGlobe,
  FaLaptopCode,
} from "react-icons/fa";
import { IoMdAnalytics } from "react-icons/io";
import { motion, useAnimation } from "framer-motion";
import { useInView } from "react-intersection-observer";

const containerVariants = {
  hidden: {
    opacity: 0,
    scale: 0.9, // Slightly scale down
  },
  visible: {
    opacity: 1,
    scale: 1, // Scale back to original size
    transition: {
      staggerChildren: 0.5,
      when: "beforeChildren", // Ensure container scales up before children animations
      ease: "easeOut", // Smooth easing function
      duration: 0.5, // Duration of the animation
    },
  },
};

const childVariants = {
  hidden: {
    opacity: 0,
    y: 50, // Start below their final position
  },
  visible: {
    opacity: 1,
    y: 0, // Move to final position
    transition: {
      type: "spring", // Spring physics for a natural feel
      stiffness: 100, // Spring stiffness, adjust for effect
      damping: 10, // Spring damping, adjust for effect
    },
  },
};
const headerVariants = {
  hidden: { opacity: 0, y: -20 },
  visible: {
    opacity: 1,
    y: 0,
    transition: { delay: 0.5, duration: 1 },
  },
};

const paragraphVariants = {
  hidden: { opacity: 0, y: 20 },
  visible: {
    opacity: 1,
    y: 0,
    transition: { delay: 1, duration: 1 },
  },
};

const gridVariants = {
  hidden: { opacity: 0 },
  visible: {
    opacity: 1,
    transition: {
      delay: 1.5,
      staggerChildren: 0.5,
    },
  },
};

const services = [
  {
    title: "SEO OPTIMIZATION",
    description:
      "Enhance your online visibility and search engine rankings with our expert SEO optimization strategies. We'll help your website shine in organic search results and drive targeted traffic to your site.",
    icon: <FaSearch className="w-10 h-10 text-rose-500" />,
  },
  {
    title: "SOCIAL MEDIA MARKETING",
    description:
      "Create a buzz on social media platforms like Facebook, Twitter, and LinkedIn. Our tailored marketing strategies will help you connect with your audience, increase brand awareness, and drive conversions.",
    icon: <FaGlobe className="w-10 h-10 text-blue-600" />,
  },
  {
    title: "EMAIL MARKETING",
    description:
      "Engage, delight, and retain your audience with our email marketing prowess. Our personalized email campaigns are designed to spark interest, build loyalty, and generate impressive results.",
    icon: <FaRegEnvelope className="w-10 h-10 text-red-500" />,
  },
  {
    title: "CREATIVE WEB DESIGN",
    description:
      "Elevate your online presence with a visually stunning website. Our creative web design services offer a captivating user experience and tell your brand's story effectively.",
    icon: <FaLaptopCode className="w-10 h-10 text-green-500" />,
  },
];

const Insights = () => {
  const controls = useAnimation();
  const [ref, inView] = useInView({
    triggerOnce: true,
    // You can adjust the threshold to control when the animation starts
    threshold: 0.1,
  });

  useEffect(() => {
    if (inView) {
      controls.start("visible");
    } else {
      controls.start("hidden");
    }
  }, [controls, inView]);
  return (
    <motion.div
      ref={ref}
      variants={containerVariants}
      initial="hidden"
      animate={controls}
      className="font-maven bg-gradient-to-br from-blue-100 to-indigo-100 text-gray-700 body-font"
    >
      <div className="container px-5 py-16 mx-auto">
        <div className="text-center mb-10">
          <motion.h1
            variants={headerVariants}
            className="text-4xl font-semibold text-indigo-600 mb-4"
          >
            Elevating Your Digital Presence
          </motion.h1>
          <motion.p
            variants={paragraphVariants}
            className="text-lg lg:text-xl leading-relaxed xl:w-2/3 lg:w-3/4 mx-auto"
          >
            We're here to empower your online success. Our mission is to
            revitalize user-centric e-markets, ignite client-focused
            initiatives, and innovate with unwavering commitment to intellectual
            excellence and top-tier results.
          </motion.p>
        </div>
        <motion.div
          variants={gridVariants}
          className="grid grid-cols-1 md:grid-cols-2 gap-6"
        >
          {services.map((service, index) => (
            <ServiceCard key={index} service={service} index={index} />
          ))}
        </motion.div>
      </div>
    </motion.div>
  );
};

const ServiceCard = ({ service, index }) => {
  // Determine the class for rounding based on the index
  const isEven = index % 2 === 0;
  const roundedClass = isEven
    ? "rounded-lg lg:rounded-l-full lg:pl-16"
    : "rounded-lg lg:rounded-r-full";

  return (
    <motion.div
      variants={childVariants}
      className={`bg-white p-8 shadow-md hover:shadow-lg ${roundedClass}`}
    >
      {service.icon}
      <h2 className="text-xl font-medium text-indigo-600 mt-4">
        {service.title}
      </h2>
      <p className="leading-relaxed text-base mt-2">{service.description}</p>
    </motion.div>
  );
};

export default Insights;
