import '@/styles/globals.css'
import LayoutComponent from '../Components/Layout';
export default function App({ Component, pageProps }) {
  return(<>
  <LayoutComponent>
      <Component {...pageProps} />
    </LayoutComponent>
  </>) 
}
