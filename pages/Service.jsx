import React from "react";
import { motion } from "framer-motion";

const services = [
  {
    name: "Gold Plan",
    description:
      "The Gold Plan offers exclusive access to premium features, including 24/7 customer support, enhanced security options, and advanced analytics tools. Tailored for businesses looking for an edge in efficiency and productivity, this plan ensures you stay ahead with the latest features and updates.",
    image: "/assets/goldplan.jpeg", // Replace with your actual image path
    features: [
      "Priority access to new features",
      "Round-the-clock technical support",
      "Advanced data protection",
      "Comprehensive analytics dashboard",
    ],
    cta: "Contact us for a custom quote tailored to your business needs.",
  },
  {
    name: "Bid Ask",
    description:
      "Our Bid Ask service provides real-time market data, offering insights into bid and ask prices to inform your trading decisions. With a focus on transparency and speed, you get access to the information you need, when you need it, to make informed trading choices.",
    image: "/assets/bidask.jpeg", // Replace with your actual image path
    features: [
      "Real-time bid/ask updates",
      "Access to historical data for analysis",
      "Customizable alerts for price thresholds",
      "Integration with major trading platforms",
    ],
    cta: "Start your free trial today and experience the difference.",
  },
  {
    name: "Buy and Sell",
    description:
      "The Buy and Sell platform is designed for seamless trading experiences, offering advanced security features and intuitive interfaces. Whether you’re buying or selling, our platform provides the tools and information you need for effective and secure transactions.",
    image: "/assets/buysell.jpeg", // Replace with your actual image path
    features: [
      "User-friendly platform for all levels of traders",
      "Advanced security measures to protect your investments",
      "Quick and easy setup process",
      "Dedicated support team for any inquiries",
    ],
    cta: "Join now and get started with a special newcomer offer.",
  },
];

const containerVariants = {
  hidden: { scale: 0.95, rotate: -5, opacity: 0 },
  visible: {
    rotate: 0,
    scale: 1,
    opacity: 1,
    transition: {
      type: "spring",
      stiffness: 100,
      mass: 0.75,
      damping: 10,
      when: "beforeChildren",
      staggerChildren: 0.3,
    },
  },
};

const imageVariants = {
  hidden: { scale: 0.85, opacity: 0, x: -100, rotate: -10 },
  visible: {
    scale: 1,
    opacity: 1,
    x: 0,
    rotate: 0,
    transition: {
      duration: 1.5, // Extended duration for a smooth, gradual effect
      ease: [0.16, 1, 0.3, 1], // Easing for a smooth and elastic feel
    },
  },
};

const contentVariants = {
  hidden: { scale: 0.95, opacity: 0, y: 30 },
  visible: {
    scale: 1,
    opacity: 1,
    y: 0,
    transition: {
      duration: 1.5, // Ensuring a smooth, gradual transition
      ease: [0.22, 1, 0.36, 1], // Smooth and sophisticated easing curve
      delay: 0.3, // Sequential appearance after the image
    },
  },
};

const buttonVariants = {
  hidden: { scale: 0.9, opacity: 0, rotate: -10 },
  visible: {
    scale: 1,
    opacity: 1,
    rotate: 0,
    transition: {
      duration: 1.2, // Quick enough to remain interactive but smooth
      ease: [0.22, 1, 0.36, 1], // Easing for a smooth and elastic feel
      delay: 0.6, // Allows the content to settle before the button makes its entrance
    },
  },
};

const Service = () => {
  return (
    <div className="py-12 bg-gradient-to-r from-gray-100 to-blue-50">
      <div className="container font-maven mx-auto px-4">
        <div className="text-center mb-16">
          <h1 className="text-4xl font-bold text-gray-800 mb-4">
            Our Services
          </h1>
          <p className="text-md text-gray-600">
            Explore our comprehensive range of services designed to meet your
            diverse needs.
          </p>
        </div>
        {services.map((service, index) => (
          <motion.div
            key={service.name}
            variants={containerVariants}
            initial="hidden"
            whileInView="visible"
            viewport={{ once: true }}
            className={`flex flex-wrap ${
              index % 2 === 0 ? "md:flex-row" : "md:flex-row-reverse"
            } items-center mb-16 bg-white rounded-lg shadow-xl overflow-hidden`}
          >
            <motion.div className="w-full md:w-1/2" variants={imageVariants}>
              <img
                src={service.image}
                alt={service.name}
                className="object-fit lg:h-[34rem] w-full"
              />
            </motion.div>
            <motion.div
              className="w-full md:w-1/2 p-8"
              variants={contentVariants}
            >
              <h2 className="text-2xl font-bold text-gray-800 mb-4 text-justify">
                {service.name}
              </h2>
              <p className="text-md text-gray-600 mb-4 text-justify">
                {service.description}
              </p>
              <ul className="list-disc pl-5 mb-4">
                {service.features.map((feature, index) => (
                  <li key={index} className="text-md text-gray-600 mb-1">
                    {feature}
                  </li>
                ))}
              </ul>
              <motion.button
                variants={buttonVariants}
                whileHover={{ scale: 1.05 }}
                whileTap={{ scale: 0.95 }}
                className="bg-gradient-to-r from-blue-500 to-teal-400 text-white py-2 px-4 rounded-lg shadow-lg  hover:shadow-xl hover:from-blue-600 hover:to-teal-500"
              >
                Join Now
              </motion.button>
            </motion.div>
          </motion.div>
        ))}
      </div>
    </div>
  );
};

export default Service;
