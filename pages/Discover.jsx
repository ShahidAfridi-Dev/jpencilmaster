import React from 'react';
import { motion, useAnimation } from 'framer-motion';
import { useInView } from 'react-intersection-observer';

const Discover = () => {
  const controls = useAnimation();
  const [ref, inView] = useInView();

  React.useEffect(() => {
    if (inView) {
      controls.start("visible");
    }
  }, [controls, inView]);

  const fadeInUp = {
    hidden: { opacity: 0, y: 30 },
    visible: {
      opacity: 1,
      y: 0,
      transition: {
        duration: 2, // Increased duration for a slower animation
        ease: "easeInOut",
      },
    },
  };

  return (
    <motion.div 
      className="bg-gradient-to-r from-blue-50 to-blue-100 text-gray-700"
      ref={ref}
      initial="hidden"
      animate={controls}
      variants={fadeInUp}
    >
      <div className="font-maven container mx-auto flex flex-col px-4 py-8 md:p-16 md:flex-row items-center justify-between space-y-8 md:space-y-0">
        
        <motion.div className="flex-1" variants={fadeInUp}>
          <img src="/assets/discover.png" alt="Illustration" className="w-full max-w-lg mx-auto transition-transform hover:scale-105" />
        </motion.div>

        <motion.div className="flex-1 space-y-5 md:space-y-6" variants={fadeInUp}>
          <motion.h1 className="text-2xl md:text-2xl lg:text-4xl font-semibold leading-snug" variants={fadeInUp}>
            Discover how we can help you to grow your business fast.
          </motion.h1>
          <motion.p className="text-lg md:text-xl leading-relaxed" variants={fadeInUp}>
            Explore our innovative solutions and personalized services designed to boost your business's growth and success.
          </motion.p>
          <motion.ul className="space-y-2 text-lg" variants={fadeInUp}>
            <motion.li className="flex items-center" variants={fadeInUp}>
              <span className="text-blue-500 mr-2">✓</span>
              Tailored strategies to scale your operations.
            </motion.li>
            <motion.li className="flex items-center" variants={fadeInUp}>
              <span className="text-blue-500 mr-2">✓</span>
              Comprehensive analytics for informed decision-making.
            </motion.li>
            <motion.li className="flex items-center" variants={fadeInUp}>
              <span className="text-blue-500 mr-2">✓</span>
              Cutting-edge technology to stay ahead of the curve.
            </motion.li>
          </motion.ul>
          <motion.button 
            className="bg-blue-600 hover:bg-blue-700 text-white font-medium py-2 px-5 rounded-md shadow-md hover:shadow-lg "
            variants={fadeInUp}
          >
            Discover More
          </motion.button>
        </motion.div>

      </div>
    </motion.div>
  );
};

export default Discover;
