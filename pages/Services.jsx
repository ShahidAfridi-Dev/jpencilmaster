import React from "react";
import {
  FaGem,
  FaRegGem,
  FaHammer,
  FaGavel,
  FaExchangeAlt,
} from "react-icons/fa"; // Import icons from react-icons
import { motion, useAnimation } from "framer-motion";
import { useInView } from "react-intersection-observer";
// Enhanced services data with more details
const services = [
  {
    id: "gold-plans",
    icon: <FaRegGem size={30} />,
    bgColor: "bg-gradient-to-br from-yellow-400 via-yellow-500 to-yellow-600",
    title: "Gold Plans",
    subtitle: "Exclusive Investments",
    description:
      "Unlock premium investment opportunities with high-yield gold plans",
    action: "Invest Now",
  },
  {
    id: "bid-ask",
    icon: <FaHammer size={30} />,
    bgColor: "bg-gradient-to-br from-red-400 via-red-500 to-red-600",
    title: "Bid and Ask",
    subtitle: "Real-time Trading",
    description:
      "Engage in the dynamic world of real-time stock trading with advanced analytics and personalized alerts.",
    action: "Start Bidding",
  },
  {
    id: "buy-sell",
    icon: <FaExchangeAlt size={30} />,
    bgColor: "bg-gradient-to-br from-green-400 via-green-500 to-green-600",
    title: "Buy and Sell",
    subtitle: "Marketplace Dynamics",
    description:
      "Experience a seamless buying and selling environment with our secure, user-friendly trading platform.",
    action: "Trade Now",
  },
];

const containerVariants = {
    visible: {
      opacity: 1,
      transition: {
        when: "beforeChildren",
        staggerChildren: 0.2,
      },
    },
    hidden: { opacity: 0 },
  };
  
  const itemVariants = {
    visible: { 
      opacity: 1, 
      y: 0, 
      rotate: 0,
      scale: 1,
      transition: { duration: 0.5 } 
    },
    hidden: { 
      opacity: 0, 
      y: 20, 
      rotate: -10,
      scale: 0.95 
    },
  };

// Card component with additional UI enhancements
const Card = ({ icon, bgColor, title, subtitle, description, action }) => (
  <motion.div
    className="relative p-8 bg-white bg-opacity-80 rounded-2xl shadow-xl hover:shadow-2xl "
    variants={itemVariants}
  >
    {/* Icon container */}
    <motion.div
      className={`absolute -top-10 left-1/2 transform -translate-x-1/2 w-20 h-20 ${bgColor} rounded-full flex items-center justify-center ${bgColor} text-white shadow-lg`}
    >
      {icon}
    </motion.div>
    {/* Card content */}
    <div className="mt-8 text-center">
      <h3 className="text-2xl font-bold mb-2">{title}</h3>
      <h4 className="text-md text-gray-500 mb-4">{subtitle}</h4>
      <p className="text-gray-600 mb-6">{description}</p>
      <motion.button
        className="mt-4 bg-gradient-to-br from-blue-500 to-blue-700 text-white font-bold py-2 px-4 rounded-md hover:bg-gradient-to-bl focus:outline-none focus:shadow-outline"
        whileHover={{ scale: 1.1 }}
      >
        {action}
      </motion.button>
    </div>
  </motion.div>
);

const Services = () => {
    const controls = useAnimation();
    const [ref, inView] = useInView({
      triggerOnce: true,
      threshold: 0.1 // Trigger the animation when 10% of the element is in view
    });
  
    React.useEffect(() => {
      if (inView) {
        controls.start("visible");
      } else {
        controls.start("hidden");
      }
    }, [controls, inView]);
  

  return (
    <>
    <div className="bg-gray-100 py-12">
      <div className="container mx-auto px-6">
        <h2 className="text-3xl font-maven font-bold text-center text-gray-800 mb-16">
          Our Exclusive Services
        </h2>
        <motion.div
          className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-10"
          variants={containerVariants}
          initial="hidden"
          animate={controls}
          ref={ref}
        >
          {services.map((service) => (
            <motion.div
              key={service.id}
              className="font-maven"
              variants={itemVariants}
            >
              <Card {...service} />
            </motion.div>
          ))}
        </motion.div>
      </div>
    </div>
  </>
  );
};

export default Services;
