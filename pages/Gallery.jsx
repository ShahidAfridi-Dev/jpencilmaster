import React from 'react';
import { motion } from 'framer-motion';

const containerVariants = {
    hidden: { opacity: 0, scale: 0.9 },
    show: {
      opacity: 1,
      scale: 1,
      transition: {
        staggerChildren: 0.1,
        when: "beforeChildren",
        ease: "easeOut",
        duration: 0.5,
      },
    },
  };
  
  const childVariants = {
    hidden: { 
      scale: 0.5,
      opacity: 0,
      rotate: -30,
    },
    show: {
      scale: 1,
      opacity: 1,
      rotate: 0,
      transition: {
        type: "spring",
        stiffness: 120,
        damping: 15,
        mass: 1,
        duration: 1,
      },
    },
  };

const Gallery = () => {
  return (
    <motion.div 
      className="font-maven max-w-7xl my-2 mx-auto p-8 bg-white rounded-lg "
      initial="hidden"
      animate="show"
      variants={containerVariants}
    >
      <section className="text-center mb-12">
        <h1 className="text-4xl font-bold text-gray-900 mb-6">Gallery </h1>
        <p className="text-gray-700 text-lg">
  Our Gallery component, integral to the website builder, stands at the forefront of innovation, 
  allowing seamless management and exhibition of your images in an elegantly structured layout. 
   It guarantees that your images 
  are showcased in the most flattering light. Notably, any images uploaded via the website builder 
  are exclusively stored in the Gallery, reinforcing a streamlined and centralized media management system, 
  thereby eliminating local storage clutter.
</p>

      </section>

      <motion.section 
        className="grid md:grid-cols-2 xl:grid-cols-3 gap-8"
        variants={containerVariants}
      >
        {/* Feature: Intuitive Upload Process */}
        <motion.div className="flex flex-col items-center text-center space-y-4" variants={childVariants}>
          <img src="/assets/upload.gif" alt="Upload" className="w-48 h-32 object-cover mb-3" />
          <h2 className="text-xl font-semibold text-gray-800">Intuitive Upload Process</h2>
          <p className="text-gray-600">
            Easily upload your images using the drag-and-drop functionality or the file selector.
          </p>
        </motion.div>

        {/* Feature: Dynamic Aspect Ratio Adjustment */}
        <motion.div className="flex flex-col items-center text-center space-y-4" variants={childVariants}>
          <img src="/assets/aspect.gif" alt="Aspect Ratio" className="w-48 h-32 object-cover mb-3" />
          <h2 className="text-xl font-semibold text-gray-800">Dynamic Aspect Ratio Adjustment</h2>
          <p className="text-gray-600">
            Select the desired aspect ratio for your images, ensuring they are displayed correctly.
          </p>
        </motion.div>

        {/* Feature: Accessibility */}
        <motion.div className="flex flex-col items-center text-center space-y-4" variants={childVariants}>
          <img src="/assets/security.gif" alt="Accessibility" className="w-48 h-32 object-cover mb-3" />
          <h2 className="text-xl font-semibold text-gray-800">Accessibility</h2>
          <p className="text-gray-600">
            Our goal is to provide a barrier-free experience, allowing users of all abilities to navigate and enjoy our gallery with ease.
          </p>
        </motion.div>

        {/* Feature: Seamless Navigation */}
        <motion.div className="flex flex-col items-center text-center space-y-4" variants={childVariants}>
          <img src="/assets/navigation.gif" alt="Seamless Navigation" className="w-48 h-32 object-contain mb-3" />
          <h2 className="text-xl font-semibold text-gray-800">Seamless Navigation</h2>
          <p className="text-gray-600">
            Navigate through your collections with ease using our fluid, intuitive interface.
          </p>
        </motion.div>

        {/* Feature: High-Quality Image Rendering */}
        <motion.div className="flex flex-col items-center text-center space-y-4" variants={childVariants}>
          <img src="/assets/quality.gif" alt="High-Quality Image Rendering" className="w-48 h-32 object-cover mb-3" />
          <h2 className="text-xl font-semibold text-gray-800">High-Quality Image Rendering</h2>
          <p className="text-gray-600">
            Experience your images in the best quality with high-resolution support and optimized loading.
          </p>
        </motion.div>

        {/* Feature: Lightweight & Fast Loading */}
        <motion.div className="flex flex-col items-center text-center space-y-4" variants={childVariants}>
          <img src="/assets/fast-loading.gif" alt="Lightweight & Fast Loading" className="w-48 h-32 object-contain mb-3" />
          <h2 className="text-xl font-semibold text-gray-800">Lightweight & Fast Loading</h2>
          <p className="text-gray-600">
            Designed for performance, our gallery ensures quick load times and a smooth experience, even with large image libraries.
          </p>
        </motion.div>
      </motion.section>
    </motion.div>
  );
};

export default Gallery;
