import React from "react";

import Collage from "./Collage";
import Discover from "./Discover";
import Banner from "./Banner";
import Services from "./Services";
import Motivation from "./Motivation";
import Insights from "./Insights";
import Community from "./Community";

const Home = () => (
  <div>
    {/* Start Banner Components */}
    <Banner />
    {/* End Banner Components */}

    {/* Start Motivational Component */}
    <Motivation />
    {/* End Motivational Component */}
    {/* Start More discover component */}
    <Discover />
    {/* End More discover component */}

    {/* Start Our Services  */}
    <Services />
    {/* End Our services section */}
{/* <Community/> */}
    {/* Start Collage component */}
    <Collage />
    {/* End Collage component */}

    <Insights />
  </div>
);

export default Home;
