    // Pricing.js
    import React from 'react';
    import { FaCheck } from 'react-icons/fa';
    import { motion } from 'framer-motion';
  // Variants for the container
  const containerVariants = {
    hidden: { 
        opacity: 0,
        x: 20 
    },
    visible: {
        opacity: 1,
        y: 20,
        transition: {
            duration: 0.8, // Slower transition
            ease: "easeInOut", // Smooth transition
            when: "beforeChildren",
            staggerChildren: 0.3
        }
    }
};

// Variants for child elements
const childVariants = {
  hidden: { 
      opacity: 0 ,
      
  },
  visible: { 
    opacity: 1,
    y: 0,
    transition: {
        duration: 2, // Much slower transition for smoothness
        ease: "easeInOut" // Gradual acceleration and deceleration
    }
}
};
  
    const PlanCard = ({ title, price, features, isHighlighted }) => {
        // Adjusted sizes for the circle and card
        const circleSize = 'w-60 h-40'; // Width and height for the circle
        const cardPadding = 'pt-28 pb-8 px-8'; // Padding for the card content
      
        return (
          <motion.div  variants={containerVariants}
          initial="hidden"
          animate="visible" className={`relative text-center rounded-3xl shadow-xl overflow-hidden ${isHighlighted ? 'bg-purple-600 text-white' : 'bg-white text-gray-800'} mx-4 my-8`}>
            {/* Circle with price */}
            <div  className={`absolute inset-x-0 top-0 mx-auto ${circleSize} rounded-full ${isHighlighted ? 'bg-purple-700' : 'bg-purple-200'} flex flex-col items-center justify-center transform -translate-y-1/2`}>
        <h2 className="font-bold text-lg mt-20">{title}</h2>
        <span className="font-bold text-xl font-poppins">{price}</span>
      </div>
            {/* Card content */}
            <motion.div variants={childVariants} className={`${cardPadding}`}>
          
              <ul className="text-left">
                {features.map((feature, index) => (
                  <li key={index} className="flex items-center mt-2">
                    <FaCheck className={`mr-2 ${isHighlighted ? 'text-white': 'text-purple-700'}`} /> {feature}
                  </li>
                ))}
              </ul>
              <button className={`mt-8 w-full py-3 rounded-full text-sm font-semibold ${isHighlighted ? 'bg-white text-purple-700' : 'bg-purple-700 text-white'}`}>
                SELECT PLAN
              </button>
            </motion.div>
          </motion.div>
        );
      };
      
    const Pricing = () => {
    const plans = [
        {
        title: 'MONTHLY',
        price: '₹ 5,000',
        features: ['Gallery', 'Website Builder', 'Custom Pages', 'Own Products Upload', 'Analytics'],
        isHighlighted: false,
        },
        {
        title: 'BI-ANNUAL',
        price: '₹ 27,500',
        features: ['Gallery', 'Website Builder', 'Pay only for 5.5 months', 'Save 9% !', 'Customer Care Support'],
        isHighlighted: true,
        },
        {
        title: 'ANNUAL',
        price: '₹ 50,000',
        features: ['Gallery', 'Website Builder', 'Pay only for 10 months', 'Save 17% !', 'Customer Care Support'],
        isHighlighted: false,
        },
    ];

    return (
        <div className="font-maven"> 
        
      <div className="flex flex-wrap justify-center gap-6 px-4 py-8 bg-indigo-500 bg-opacity-30 backdrop-blur-md ">
      <div className="container mx-auto text-center">
                <h1 className="text-3xl font-bold mb-4">Choose Your Plan</h1>
                <p className="text-lg text-gray-700">Select the best plan that fits your needs. From monthly subscriptions to annual plans, we offer a variety of options to suit your business. Each plan comes with its own set of features designed to help you grow.</p>
            </div>
        {plans.map((plan, index) => (
            <PlanCard key={index} {...plan} />
        ))}
        </div>
        </div>
    );
    };

    export default Pricing;
