import React from 'react';

const Pricing = () => {
  const plans = [
    { name: 'Basic', price: 199, features: ['50GB Disk Space', '50GB Bandwidth', '20 Email Accounts', 'Subdomains', 'Maintenance'], popular: false },
    { name: 'Standard', price: 299, features: ['50GB Disk Space', '50GB Bandwidth', '20 Email Accounts', '20 Subdomains', 'Maintenance'], popular: true },
    { name: 'Premium', price: 399, features: ['50GB Disk Space', '50GB Bandwidth', '20 Email Accounts', '30 Subdomains', 'Maintenance'], popular: false },
  ];

  const CheckIcon = () => (
    <svg className="h-6 w-6 text-green-500" fill="none" viewBox="0 0 24 24" stroke="currentColor">
      {/* Replace with an actual checkmark SVG path */}
    </svg>
  );

  const CrossIcon = () => (
    <svg className="h-6 w-6 text-red-500" fill="none" viewBox="0 0 24 24" stroke="currentColor">
      {/* Replace with an actual 'x' SVG path */}
    </svg>
  );

  return (
    <div className="bg-white py-12 px-4 sm:px-6 lg:px-8">
      <div className="mx-auto max-w-7xl text-center">
        <h2 className="text-3xl font-extrabold text-gray-900 sm:text-4xl">Choose Your Pricing Plan</h2>
        <p className="mt-4 text-xl text-gray-500">All plans FREE for the first 30days</p>

        <div className="mt-10 space-y-4 sm:space-y-0 sm:grid sm:grid-cols-3 sm:gap-5 lg:gap-8">
          {plans.map((plan, index) => (
            <div key={index} className={`relative p-6 border-2 ${plan.popular ? 'border-blue-500' : 'border-gray-300'} rounded-lg bg-white shadow-lg flex flex-col`}>
              {plan.popular && (
                <div className="ribbon bg-blue-500 text-white px-3 py-1 text-sm font-bold absolute top-0 right-0 transform translate-x-1/3 -translate-y-1/3 rotate-45">
                  Popular
                </div>
              )}
              <h3 className={`text-lg leading-6 font-medium ${plan.popular ? 'text-blue-600' : 'text-gray-900'}`}>{plan.name}</h3>
              <p className="mt-4 flex items-baseline text-6xl font-extrabold">
                ${plan.price}
                <span className="ml-1 text-2xl font-medium text-gray-500">/month</span>
              </p>
              <ul role="list" className="mt-6 flex-1 space-y-4">
                {plan.features.map((feature, featureIndex) => (
                  <li key={featureIndex} className="flex items-start">
                    <div className="flex-shrink-0">
                      {feature.includes('Maintenance') ? <CrossIcon /> : <CheckIcon />}
                    </div>
                    <p className="ml-3 text-base text-gray-700">{feature}</p>
                  </li>
                ))}
              </ul>
              <button
                type="button"
                className={`mt-8 w-full bg-${plan.popular ? 'blue-600' : 'gray-800'} border border-transparent rounded-md py-2 px-4 flex items-center justify-center text-base font-medium text-white hover:bg-${plan.popular ? 'blue-700' : 'gray-900'}`}
              >
                Choose Plan
              </button>
            </div>
          ))}
       
        </div>
      </div>
    </div>
  );
};

export default Pricing;

