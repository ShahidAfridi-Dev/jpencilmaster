import React from 'react'
import Image from 'next/image';
import { motion } from 'framer-motion';

const Banner = () => {
  // Variants for the animation
  const imageVariants = {
    hidden: { opacity: 0 },
    visible: {
      opacity: 1,
      transition: {
        duration: 1, // A gentle and smooth transition duration
        ease: "easeIn", // A smooth easing function for a more natural effect
      },
    },
  };

  return (
    <>
    <motion.div className="relative w-full pt-[56.25%] md:pt-[30%] lg:h-screen"
    initial="hidden"
    animate="visible"
    variants={imageVariants}
  >
    <Image
      src="/assets/banner1.jpeg"
      alt="Grand Mosque"
      layout="fill"
      objectFit="cover"
      quality={100}
      className="z-0 absolute top-0 left-0 w-full h-full"
    />
</motion.div>

    </>
  )
}

export default Banner;
