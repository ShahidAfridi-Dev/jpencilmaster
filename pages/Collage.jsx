import React from "react";
import { motion, useAnimation } from 'framer-motion';
import { useInView } from 'react-intersection-observer';

const Collage = () => {
    const controls = useAnimation();
    const [ref, inView] = useInView({
        threshold: 0.2,  // Trigger the animation when the component is 20% visible
        triggerOnce: true  // Ensures the animation only occurs once
    });

    React.useEffect(() => {
        if (inView) {
            controls.start('visible');
        }
    }, [controls, inView]);

    const itemVariants = {
        hidden: { x: -100, opacity: 0, scale: 0.9 },
        visible: {
            x: 0,
            opacity: 1,
            scale: 1,
            transition: {
                x: { 
                    type: "spring", 
                    stiffness: 50, // Lower stiffness for a softer spring
                    damping: 20,   // Higher damping for less oscillation
                    duration: 1.5  // Increase duration for a slower animation
                },
                opacity: { 
                    duration: 1.5, // Sync opacity transition with movement
                    ease: "easeOut" // Use an 'easeOut' for a smoother fade in
                },
                scale: {
                    duration: 1.5,
                    ease: "easeOut" // Apply the same easing to scale
                }
            }
        }
    };
    

    return (
        <div className="container font-maven mx-auto p-6">
        <motion.div
         className="bg-gradient-to-r from-purple-500 to-indigo-600 rounded-lg shadow-2xl p-10 text-white"
            ref={ref}
            initial="hidden"
            animate={controls}
            variants={{
                hidden: { opacity: 0, scale: 0.8 },
                visible: { opacity: 1, scale: 1, transition: { duration: 0.8, staggerChildren: 0.2 } }
            }}
        >
            <div className="flex flex-col md:flex-row justify-between items-center">
                <div className="mb-6 md:mb-0 md:w-1/2">
                    <motion.h1
                        className="text-xl md:text-2xl lg:text-3xl font-extrabold leading-snug mb-6"
                        variants={itemVariants}
                    >
                        Unveil Your Digital Empire in a Swift 40 Minutes!
                    </motion.h1>
                    <motion.p
                        className="mb-6 md:mb-8 text-lg md:text-2xl font-medium"
                        variants={itemVariants}
                    >
                        Begin your entrepreneurial journey with no initial investment and seamlessly integrate into the digital market. Join a network of innovative and successful business owners.
                    </motion.p>
                    <motion.button
                        className="bg-transparent border-2 border-white text-xl hover:bg-white hover:text-blue-600 font-semibold py-3 px-6 rounded-lg"
                        variants={itemVariants}
                        whileHover={{ scale: 1.05 }}
                        whileTap={{ scale: 0.95 }}
                    >
                        Start Your Success Story Today!
                    </motion.button>
                </div>
                <motion.div className="md:w-1/2" variants={itemVariants}>
                    <motion.img
                        className="rounded-xl shadow-2xl"
                        src="/assets/collage.jpeg"
                        alt="Screenshot of Business Platform"
                        whileHover={{ scale: 1.03 }}
                    />
                </motion.div>
            </div>
        </motion.div>
    </div>
    
    );
};

export default Collage;
